var Decidr = (function() {
  var information = {
    name: "Decidr",
    description: "A small library to help people make better choices.",
    version: 0.1,
    author: "idfive",
    uri: "http://www.idfive.com"
  };

  /**
   * Calculates option scores based on users input
   * @param {Array} answers
   * @param {Array} options
   */
  function calculateScores(answers, options) {
    var calculateResults = [];
    options.forEach(function(option) {
      var score = 0;
      var scoresArray = answers.map(function(x) {
        return option.scores[x];
      });
      score = scoresArray.reduce(function(accumulator, current) {
        return accumulator + current;
      });
      option["userScore"] = score;
      calculateResults.push(option);
    });

    return calculateResults;
  }

  /**
   * Sort options by score in DESC order
   * @param {Array} options
   */
  function sortByScore(options) {
    return options.sort(function(a, b) {
      return a.userScore < b.userScore;
    });
  }

  return {
    info: information,
    limit: 3,
    run: function(answers, options, limit) {
      var start = performance.now();
      var results = calculateScores(answers, options);
      results = sortByScore(results).slice(0, this.limit);
      var complete = performance.now();
      console.log("Execution Time: " + (complete - start) + "ms");
      return results;
    },

    init: function(opt) {
      var options = opt.options;
      var answers = opt.answers;
      // If limit is defined, overwrite default
      if (opt.limit !== undefined) {
        this.limit = opt.limit;
      }
      // Run
      return this.run(answers, options, this.limit);
    }
  };
})();
