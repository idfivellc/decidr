# DecidR
A small library to help people make better choices.

![George-W-Bush.jpg](https://bitbucket.org/repo/GgpRLGR/images/1588553106-George-W-Bush.jpg)

## Usage

```
var Programs = new Decidr({
  answers: [a1, b2, c3], // User selected options
  dataset: [{
      id: 0,
      label: 'Go back to school for Masters',
      info: {
        // custom information about option.
      }
      scores: {
        a0: 0,
        a1: 0.1,
        a2: 0,
        b0: 0.9,
        b1: 0.1,
        b2: 0.3,
        c0: 0.1,
        c1: 0.5,
        c2: 0
      }
    },
    ...
  ],
  limit: 3 // Default
})
```
