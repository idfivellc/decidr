"use strict";
var sampleData = [
  {
    id: 0,
    label: "Lamborghini",
    scores: {
      a0: 0.2,
      a1: 0.2,
      a2: 0.1,
      b0: 0.75,
      b1: 0,
      b2: 0,
      c0: 0,
      c1: 0,
      c2: 0.8
    }
  },
  {
    id: 1,
    label: "2018 Land Rover Range Rover Sport Supercharged",
    scores: {
      a0: 0.4,
      a1: 0.2,
      a2: 0.1,
      b0: 0.6,
      b1: 0.6,
      b2: 0.3,
      c0: 0.3,
      c1: 0.7,
      c2: 0.2
    }
  },
  {
    id: 2,
    label: "2016 Nissan Quest",
    scores: {
      a0: 0.4,
      a1: 0.5,
      a2: 0.2,
      b0: 0.4,
      b1: 0.7,
      b2: 0.84,
      c0: 0.72,
      c1: 0.7,
      c2: 0.1
    }
  },
  {
    id: 3,
    label: "Ferrari 458",
    scores: {
      a0: 0.2,
      a1: 0.1,
      a2: 0,
      b0: 0.7,
      b1: 0,
      b2: 0,
      c0: 0.1,
      c1: 0.7,
      c2: 0.9
    }
  },
  {
    id: 4,
    label: "2018 Honda CR-V",
    scores: {
      a0: 0.3,
      a1: 0.5,
      a2: 0.8,
      b0: 0.3,
      b1: 0.4,
      b2: 0.83,
      c0: 0.42,
      c1: 0.6,
      c2: 0.2
    }
  }
];

function syntaxHighlight(json) {
  if (typeof json != "string") {
    json = JSON.stringify(json, undefined, 2);
  }
  json = json
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;");
  return json.replace(
    /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
    function(match) {
      var cls = "number";
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          cls = "key";
        } else {
          cls = "string";
        }
      } else if (/true|false/.test(match)) {
        cls = "boolean";
      } else if (/null/.test(match)) {
        cls = "null";
      }
      return '<span class="' + cls + '">' + match + "</span>";
    }
  );
}

var form = document.forms.questions;
var resultsDiv = document.getElementById("results");

form.addEventListener(
  "submit",
  function(event) {
    resultsDiv.innerHTML = "";
    var ProgramDecidr = [];
    var results = [];
    var formData = new FormData(form);

    ProgramDecidr = Decidr.init({
      answers: [formData.get("q1"), formData.get("q2"), formData.get("q3")],
      options: sampleData,
      limit: 2
    });

    resultsDiv.innerHTML = syntaxHighlight(ProgramDecidr);
    event.preventDefault();
  },
  false
);
